#!/usr/bin/env bash
# TCrpr's Unofficial ArchInstaller v1.2
# What you shouldn't expect from this installer at the moment:
# Work for BIOS systems, Create LVM or RAID partitions, choose the size of the partitions,
# Dualboot, encryption and finally.. working code
# I do not recommend you using this script on a physical computer.

msg() {
    printf "\033[1m=> $@\033[m\n"
}

msg_n() {
	printf "\033[1m=> $@\033[m"
}

msg_warn() {
    printf "\033[1m\033[33mWARNING: $@\033[m\n"
}

msg_error() {
    printf "\033[1m\033[31mERROR: $@\033[m\n"
}

msg_warn "This installer isn't stable! Do NOT use for actual system installers for now."
msg "==========================================================="
msg "~~~~~~TCreeper's Unofficial ArchInstaller version 1.2~~~~~~"
msg "==========================================================="

find /usr/share/kbd/keymaps/i386 -exec basename -s .map.gz -a {} + | column 
msg_n "Write the name of the keyboard layout you want to use, then press (Do not include before the /' or the extension example: trq)[ENTER]: "
read keymap 
clear
msg "Chosen keymap: $keymap" && cd 
loadkeys $keymap || { msg_error "Wrong keyboard layout"; exit 1; }

if [ -e "/sys/firmware/efi/efivars" ]; then
	msg "Boot mode: UEFI"
else
	msg_error "System is booted in BIOS or CSM mode. This program only works in UEFI, if your system is UEFI capable try
	disabling 'Legacy Support' or 'Compatibility Support Module (CSM)'"
	exit 1
fi	


msg "Synchronizing system clock" && timedatectl set-ntp true
wget -q --spider https://archlinux.org | msg "Network: Available" || { msg_error "Network not available"; exit 1; }
fdisk -l && 
msg "!! This installer cannot dual boot !! !! The WHOLE drive would be used !!"
msg_n "Write the path of the **drive** you want to install on, then press [ENTER]: /dev/"
read installdrive
msg "Chosen drive: /dev/$installdrive"

sizeofdrive=$( blockdev --getsize64 "/dev/$installdrive" | numfmt --to=iec-i --suffix=B --format="%.3f" | cut -f 1  -d '.' )

if [ $sizeofdrive \< 20 ]; then
  	 if [ $sizeofdrive \< 4 ]; then
   				msg_error "Drive is smaller then 4G, aborting, if you want to continue remove this line."; exit 1
   	fi
		else msg_warn " Drive is smaller then 20G, A bigger drive is recommended, installing anyway.";
fi;

if [ 80 \< $sizeofdrive ]; then
		msg_n "Should /home be seperate? Type y or n (Nothing else) [Y/n] "; read homeseperate
	
		if [ $homeseperate != n ];  then
				homeseperate=yes
			else homeseperate=no
		fi
	else homeseperate=no
fi

if [ $homeseperate = yes ]; then
   		msg "/home: Seperate from root" && sleep 1
	else msg "/home: Not seperate from root" && sleep 1
fi

msg_n "Is your cpu AMD or Intel, leave blank if you do not want to install the microcode, all lowercase (ex. intel): "
read microcodeis
case $microcodeis in
	amd|AMD) microcode=amd-ucode ;;
	intel|Intel) microcode=intel-ucode ;;
	*) msg_warn "Not installing microcode"
esac

msg "The installer will remake the partition layout of /dev/$installdrive in 10 seconds"
msg "Do Ctrl-C if you do not want this"
sleep 10
clear
umount /mnt/boot/ &> /dev/null && msg "/mnt/boot unmounted"
umount /mnt/home/ &> /dev/null && msg "/mnt/home unmounted"
umount /mnt/ &> /dev/null && msg "/mnt unmounted"

parted /dev/$installdrive mklabel gpt &&
parted -a optimal /dev/$installdrive mkpart primary 0% 550MiB &&
parted /dev/$installdrive set 1 boot on &&
parted /dev/$installdrive set 1 esp on &&

if [ $homeseperate = yes ]; then
	parted -a optimal /dev/$installdrive mkpart primary 550MiB 32GiB &&
	parted -a optimal /dev/$installdrive mkpart primary 32GiB 100% &&

	while true; do
		msg_n "What filesystem do you want / to be? [ext4/ext3/ext2/Btrfs/XFS/JFS/ReiserFS]: "; read rootfstype
		rootfstype=${rootfstype,,} &&
		if  [ "$rootfstype" = ext4  ] || \
			[ "$rootfstype" = ext3  ] || \
			[ "$rootfstype" = ext2  ] || \
			[ "$rootfstype" = btrfs ] || \
			[ "$rootfstype" = jfs   ] || \
			[ "$rootfstype" = xfs   ] || \
			[ "$rootfstype" = reiserfs ] 
		then
			msg "Root filesystem type: $rootfstype"
			return 0 
		else
			msg_error "Wrong filesystem type"
		fi
	done

	while true; do
		msg_n "What filesystem do you want /home to be? [ext4/ext3/ext2/Btrfs/XFS/JFS/ReiserFS]: "
		read homefstype
		homefstype=${homefstype,,} &&
		if  [ "$rootfstype" = ext4  ] || \
			[ "$rootfstype" = ext3  ] || \
			[ "$rootfstype" = ext2  ] || \
			[ "$rootfstype" = btrfs ] || \
			[ "$rootfstype" = jfs   ] || \
			[ "$rootfstype" = xfs   ] || \
			[ "$rootfstype" = reiserfs ] 
		then
			msg "Home filesystem type: $homefstype"
			return 0
		else
			msg_error "Wrong filesystem type"; exit 1	
		fi
	done
	rootfs="/dev/$installdrive"2
	homefs="/dev/$installdrive"3 
fi

if [ $homeseperate = no ]; then
	parted -a optimal /dev/$installdrive mkpart primary 550MiB 100%
	while true; do
		msg_n "What filesystem do you want / to be? [ext4/ext3/ext2/Btrfs/XFS/JFS/ReiserFS]: "; read rootfstype
		rootfstype=${rootfstype,,} &&
		if  [ "$rootfstype" = ext4  ] || \
			[ "$rootfstype" = ext3  ] || \
			[ "$rootfstype" = ext2  ] || \
			[ "$rootfstype" = btrfs ] || \
			[ "$rootfstype" = jfs   ] || \
			[ "$rootfstype" = xfs   ] || \
			[ "$rootfstype" = reiserfs ] 
		then
			msg "Root filesystem type: $rootfstype"
			break
		else
			msg_error "Wrong filesystem type"
		fi
	done
	rootfs="/dev/$installdrive"2
fi 


msg "Root Filesystem:" $rootfstype

msg "Creating a FAT32 filesystem for the ESP.."
filesystemtools="$filesystemtools dosfstools"
mkfs.fat -F32 /dev/"$installdrive"1 &&

if [ "$rootfstype" = btrfs ]; then
		msg "Creating a Btrfs filesystem at $rootfs"
		filesystemtools="$filesystemtools btrfs-progs"
		mkfs -t btrfs -f $rootfs &&
		msg "Created Root filesystem."
	else
		msg "Creating a '$rootfstype' filesystem at $rootfs"
		mkfs -t $rootfstype $rootfs &&
		msg "Created Root filesystem."
fi

if [ "$homefstype" = btrfs ]; then
		msg "Creating a Btrfs filesystem at $homefs"
		filesystemtools="$filesystemtools btrfs-progs"
		mkfs -t btrfs -f $homefs &&
		msg "Created home filesystem"
elif [ ! -z "$homefstype" ]; then
		msg "Creating a '$homefstype' filesystem at $homefs"
		mkfs -t $homefstype $homefs &&
		msg "Created home filesystem"		
fi

if [ "$homeseperate" = yes ]; then
		mount $rootfs /mnt
		mkdir /mnt/home
		mount $homefs /mnt/home
	else
		mount $rootfs /mnt
fi

mkdir -p /mnt/boot
mount /dev/"$installdrive"1 /mnt/boot

msg "Partitioning is done!" && sleep 2 && clear


msg_n "Now, lets choose a kernel [standard/lts/hardened/zen]: "; read kernel
case $kernel in
	lts | LTS)
		msg "Using LTS kernel"
		kernel=linux-lts
		;;	
	hardened)
		msg "Using hardened kernel"
		kernel=linux-hardened
		;;
	zen)
		msg "Using zen kernel"
		kernel=linux-zen
		;;	
	*)
		msg "Using standard kernel (linux)"
		kernel=linux
esac
msg_n "Install firmware? (RECOMMENDED) [Y/n]: "; read insfirm
case $insfirm in
	n|N) msg_warn "Not nstalling firmware" ;;
	*) firmware=linux-firmware ;;
esac



msg "If you want more packages to be installed, add them now, if you don't leave it blank"

msg "Things you might want to install:"
msg "base-devel: Has sudo, which, grep, file and some other useful tools"
msg "man-db: For accessing man pages"
msg "pacman-contrib: Some useful tools for pacman"
msg "Or you might want an editor like vim or nano"
	
msg_n "Seperate with spaces, to finish press [ENTER]: "; read extrapackages
msg "Installing the system.."
pacstrap /mnt base $filesystemtools $extrapackages $kernel $firmware

msg "Installation part complete, moving on to configuration of the system"

msg "Creating fstab via genfstab"
genfstab -U /mnt >> /mnt/etc/fstab
msg "/etc/fstab:"
cat "/mnt/etc/fstab"

msg "Setting timezone"
timezone=$(curl https://ipapi.co/timezone)
echo "Timezone: $timezone"
ln -sf /mnt/usr/share/zoneinfo/$timezone /mnt/etc/localtime

msg "Synchronizing hardware clock"
arch-chroot /mnt hwclock --systohc

msg "Setting keyboard layout to: $keymap"
echo "KEYMAP=$keymap" >> /mnt/etc/vconsole.conf

msg_n "What should the hostname of the system be?"; read hostname
echo $hostname >> /etc/vconsole.conf
msg "Hostname: $hostname"

msg "Setting up /etc/hosts"
tee /mount/etc/hosts <<EOF >/dev/null
127.0.0.1 localhost
127.0.0.1 localhost.localdomain
127.0.0.1 local
255.255.255.255 broadcasthost
::1 localhost
::1 ip6-localhost
::1 ip6-loopback
fe80::1%lo0 localhost
ff00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts
0.0.0.0 0.0.0.0
EOF

# Rebuild the initramfs
arch-chroot /mnt 'mkinitcpio -P'

msg "Set root password"
arch-chroot /mnt 'passwd'

# TODO: configure locale

# Install systemd-boot
arch-chroot /mnt 'bootctl install --path=/boot'

# Create a pacman hook to auto-update systemd-boot when a systemd update occurs
msg "Setting up systemd-boot pacman hook"
mkdir -p /mnt/etc/pacman.d/hooks
tee /mnt/etc/pacman.d/hooks/100-systemd-boot.hook <<EOF >/dev/null
[Trigger]
Type = package
Operation = Upgrade
Target = systemd

[Action]
Description = Upgrading systemd-boot
When = PostTransaction
Exec = /usr/bin/bootctl update
EOF

# Set up systemd-boots configuration files 
mkdir -p /mnt/boot/loader/entries
touch /mnt/boot/loader/loader.conf
msg "Setting up /boot/loader/loader.conf"
echo "default  arch" >> /mnt/boot/loader/loader.conf
echo "timeout 4" >> /mnt/boot/loader/loader.conf

mkdir -p /mnt/boot/loader/entries
touch /mnt/boot/loader/entries/arch.conf
msg "Setting up /boot/loader/entries/arch.conf"
echo "title Arch Linux" >> /mnt/boot/loader/entries/arch.conf
echo "linux /vmlinuz-$kernel" >> /mnt/boot/loader/entries/arch.conf
if [ -z ${microcode+x} ]; then
	echo "initrd /$microcode.img" >> /mnt/boot/loader/entries/arch.conf
fi
echo "initrd /initramfs-$kernel.img" >> /mnt/boot/loader/entries/arch.conf

msg_n "Enter extra boot options, leave blank if none. (ex. 'pci=nomsi nomodeset' ): "
read extrabootflags
msg "Boot options:"
echo "options	rw root=$rootfs $extrabootflags" >> /mnt/boot/loader/entries/arch.conf

msg "Ok i don't know how to do networking so you have to do that yourself"
msg_warn "Locale is not set, please set the locale correctly by entering the chroot and configuring it"
msg_n "Do you want to enter the chroot (c) or reboot to the new system (r)? [c/R]: "
read rebootorchroot
if [ "$rebootorchroot" = r ]; then
		systemctl reboot
	else
		exec arch-chroot /mnt
fi
