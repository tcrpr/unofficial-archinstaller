==============================================
TCrpr's Unofficial Arch Linux Installer Script
==============================================

Please read below before attempting to run the script. Also, i made this README just so i can try rst

Usage
~~~~~

    ./archinstaller.sh

**NOTE:** I do not recommend using this script on hardware. This script does not support any manual partitioning, it will overwrite the drive! The script is UEFI only.

Partitioning
~~~~~~~~~~~~

Partitioning is done automatically, If the drive is larger than 80GiB, the user is asked to seperate their home partition. 

If home is seperate:

+----------------+--------------+---------------------+-----------+
| Partition      | Size         | Filesystem          | Used as   |
+================+==============+=====================+===========+
| Partition 1    | 550MiB       | FAT32               | /boot/efi |
+----------------+--------------+---------------------+-----------+
| Partition 2    | 32218MiB     | Chosen by user [1]_ | /home     |
+----------------+--------------+---------------------+-----------+
| Partition 3    | Rest of disk | Chosen by user [1]_ | /         |
+----------------+--------------+---------------------+-----------+

If home is not seperate:

+----------------+--------------+---------------------+-----------+
| Partition      | Size         | Filesystem          | Used as   |
+================+==============+=====================+===========+
| Partition 1    | 550MiB       | FAT32               | /boot/efi |
+----------------+--------------+---------------------+-----------+
| Partition 2    | Rest of disk | Chosen by user [1]_ | /         |
+----------------+--------------+---------------------+-----------+

.. [1] Any of ext4, ext3, ext2, btrfs, jfs, xfs, reiserfs


Kernel
~~~~~~
User is asked to choose between ``linux``, ``linux-lts``, ``linux-hardened``, ``linux-zen``

Bootloader
~~~~~~~~~~
Uses systemd-boot